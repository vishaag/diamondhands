import React, { useState } from "react";
import SideBar from './components/SideBar';

export function App() {
  return (
    <>
      <header>
        <h1 className="title">
          Diamond Hands 💎 🤲
        </h1>
      </header>
      <section className="main-section">
        <SideBar />
        <div>Chart</div>
      </section>
      <footer className="footer">
        <a
          href="https://vishaag.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
        Submitted by Vishaag
        </a>
      </footer>
    </>
  );
}
